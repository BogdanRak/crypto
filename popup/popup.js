let KEY;
let IV;
let RSA;
let RSA_PUB;

function showAlert(message) {
	const alert = document.querySelector("#alert");
	const content = document.querySelector("#content");
	alert.innerText = message;
	alert.style.display = 'block';
	content.style.display = 'none';
	setTimeout(_=>{
		alert.style.display = 'none';
		content.style.display = 'block';
	}, 2000);
}

function genKey() {
	const key_aes = document.querySelector("#key_aes");
	const key_iv = document.querySelector("#key_iv");
	const data = chrome.extension.getBackgroundPage().Generate_key();
	KEY = data.key;
	IV = data.iv;
	key_aes.value = KEY.toString();
	key_iv.value = IV.toString();
}

function de_en_crypt() {
	const {callback} = this;
	const user_text = document.querySelector("#user_text");
	const key_aes = document.querySelector("#key_aes");
	const text = user_text.value;
	const keystr = key_aes.value;
	const outTextArea = document.querySelector("#out");
	if (text) {
		const data = chrome.extension.getBackgroundPage()[callback](text, KEY, IV);
		outTextArea.value = data.message;
	}
}

function ongenerate_rsa() {
	chrome.extension
		.getBackgroundPage()
		.Generate_Rsa()
		.then(data=>{
			RSA = data.rsa;
			RSA_PUB = data.rsa_pub;
			const key_rsa = document.querySelector('#key_rsa');
			key_rsa.value = RSA_PUB;
		});
}

function onencrypt_aes_iv() {
	const key_aes = document.querySelector("#key_aes");
	const key_iv = document.querySelector("#key_iv");
	if (!key_aes.value || !key_iv.value) {
		showAlert('Please add aes key and iv');
		return
	}
	if (!RSA_PUB) {
		showAlert('Please Generate Rsa Key');
		return 
	}
	const obj = {key: key_aes.value, iv: key_iv.value};
	const encrypted_rsa = chrome.extension
		.getBackgroundPage()
		.Encrypt_Rsa(JSON.stringify(obj), RSA_PUB);
	const out_rsa = document.querySelector("#out_rsa");
	out_rsa.value = encrypted_rsa;
}

function ondecrypt_rsa() {
	const out_rsa = document.querySelector("#out_rsa");
	if (!out_rsa.value) return
	const decrypted_rsa = chrome.extension
		.getBackgroundPage()
		.Decrypt_Rsa(out_rsa.value, RSA);
	out_rsa.value = decrypted_rsa;
	const obj = JSON.parse(decrypted_rsa);
	const key_aes = document.querySelector("#key_aes");
	const key_iv = document.querySelector("#key_iv");
	key_aes.value = obj.key;
	key_iv.value = obj.iv;
	KEY = chrome.extension
		.getBackgroundPage()
		.CryptoJS_hex(obj.key)
	IV = chrome.extension
		.getBackgroundPage()
		.CryptoJS_hex(obj.iv)
	console.log(KEY, IV);
}

function addListeners() {
	const generate = document.querySelector('#generate');
	if (generate) {
		generate.addEventListener('click', genKey);
	}
	const encrypt = document.querySelector('#encrypt');
	if (encrypt) {
		encrypt.addEventListener('click', de_en_crypt.bind({callback: 'Encrypt_Text'}));
	}
	const decrypt = document.querySelector('#decrypt');
	if (decrypt) {
		decrypt.addEventListener('click', de_en_crypt.bind({callback: 'Decrypt_Text'}));
	}
	const generate_rsa = document.querySelector('#generate_rsa');
	if (generate_rsa) {
		generate_rsa.addEventListener('click', ongenerate_rsa);
	}
	const encrypt_aes_iv = document.querySelector('#encrypt_aes_iv');
	if (encrypt_aes_iv) {
		encrypt_aes_iv.addEventListener('click', onencrypt_aes_iv);
	}
	const decrypt_rsa = document.querySelector('#decrypt_rsa');
	if (decrypt_rsa) {
		decrypt_rsa.addEventListener('click', ondecrypt_rsa);
	}
}

document.addEventListener("DOMContentLoaded", function(event) {
	addListeners();
});