const secret = "Secret Passphrase"

function Generate_key() {
  const salt_key = CryptoJS.lib.WordArray.random(128/8);
  const salt_iv = CryptoJS.lib.WordArray.random(128/8);
  const key = CryptoJS.PBKDF2(secret, salt_key, { keySize: 256/32 });
  const iv = CryptoJS.PBKDF2(secret, salt_iv, { keySize: 256/32 });
  return {key, iv};
}

function Encrypt_Text(message, _key, _iv) {
  const data = Generate_key();
  const key = _key || data.key;
  const iv = _iv || data.iv;
  const encrypted = CryptoJS.AES.encrypt(
    message,
    key,
    {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    }
  );
  return {message: encrypted.toString(), key, iv};
}

function Decrypt_Text(data, key, iv) {
  const decrypted = CryptoJS.AES.decrypt(
    data,
    key,
    {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    }
  );
  console.log(decrypted, decrypted.toString(CryptoJS.enc.Utf8));
  return {message: decrypted.toString(CryptoJS.enc.Utf8)}
}

function Generate_Rsa() {
  return new Promise(function (s, f){
    setTimeout(()=>{
      const rsa = cryptico.generateRSAKey("", 2048);
      const rsa_pub = cryptico.publicKeyString(rsa);
      s({rsa, rsa_pub});
    }, 10);
  });
}

function Encrypt_Rsa(text, rsa_pub) {
  return cryptico.encrypt(text, rsa_pub).cipher;
}

function Decrypt_Rsa(text, rsa) {
  const data = cryptico.decrypt(text, rsa);
  return data.plaintext
}

function CryptoJS_hex(text) {
  return CryptoJS.enc.Hex.parse(text);
}

